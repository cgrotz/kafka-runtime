Kafka-Runtime
============
This repository contains a convenient way to run a Kafka broker cluster.

## kafka-docker-image
_kafka-docker-image_ contains a Java based Kafka Executor that provides a Kafka Broker packaged as a Docker image.

The broker inside the Docker container can be configured using environment Variables. All variables starting with KAFKA_ will be put into the brokers server.properties file (e.g. KAFKA_BROKER_ID will be used to fill the broker.id property).

The container exposes the ports 9092 and 9093. The Kafka log folder (_/var/kafka/logs_) is exposed as a volume.

## Docker Compose examples
A part of this repository are multiple Docker Compose examples, that show different deployment options for a Kafka and Zookeeper cluster.

### devmode
Contains a Docker Compose yml that contains a development mode deployment. This deployment consist of a single Zookeeper Server and a single Kafka broker.

You need to replace the HOST_IP in the compose.yml with the actual IP of the host machine. You have to do this, in order for the Kafka Brokers to correctly communicate with each other in a Cluster.

### cluster
Contains a Docker Compose yml that contains a clustered deployment. This deployment consist of three Zookeeper servers and three Kafka brokers.

You need to replace the HOST_IP in the compose.yml with the actual IP of the host machine. You have to do this, in order for the Kafka Brokers to correctly communicate with each other in a Cluster.

### secured (Not ready yet)
Contains a Docker Compose yml that contains a clustered and secured deployment. This deployment consist of three Zookeeper servers and three Kafka brokers, that are secured using TLS (see truststore and keystore for the certificates). The client needs to use the example client-truststore and client-keystore in order to connect with the Kafka cluster.

### Further reading
#### Security and Authorization
http://kafka.apache.org/documentation.html#security_authz

#### Zookeeper SSL
https://cwiki.apache.org/confluence/display/ZOOKEEPER/ZooKeeper+SSL+User+Guide


#### Monitoring
https://blog.serverdensity.com/how-to-monitor-kafka/
