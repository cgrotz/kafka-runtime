Zookeeper Launcher
==================

Use environment variables to control the behavior.

BROKER_ID     = The id of the broker. This must be set to a unique integer for each broker.
ZOO_SERVERS   = This variable allows you to specify a list of machines of the Zookeeper ensemble. Each entry has the form of server.id=host:port:port. Entries are separated with space.

