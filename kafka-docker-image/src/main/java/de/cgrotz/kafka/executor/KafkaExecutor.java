package de.cgrotz.kafka.executor;

import kafka.server.KafkaServerStartable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by christoph on 07.08.17.
 */
public class KafkaExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaExecutor.class);

    public static void main(String[] args) throws IOException, URISyntaxException {
        checkPrerequisites();

        Properties properties = new Properties();
        properties.load(KafkaExecutor.class.getClassLoader()
            .getResourceAsStream("default.properties"));

        new File("/var/kafka/logs").mkdirs();
        properties.setProperty("log.dirs","/var/kafka/logs");

        mergeWithEnvironmentVariables(properties);

        final KafkaServerStartable kafkaServerStartable = KafkaServerStartable.fromProps(properties);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("kafka-shutdown-hook") {
            @Override
            public void run() {
                kafkaServerStartable.shutdown();
            }
        });

        kafkaServerStartable.startup();
        kafkaServerStartable.awaitShutdown();
    }

    private static void mergeWithEnvironmentVariables(Properties properties) {
        System.getenv().entrySet().stream()
            .filter(entry -> entry.getKey().startsWith("KAFKA_"))
            .forEach(entry -> {
                String key = entry.getKey().substring(6)
                    .toLowerCase().replaceAll("_",".");
                properties.setProperty(key, entry.getValue());
            });
    }


    private static void checkPrerequisites() throws URISyntaxException, IOException {
        if(System.getenv("ZOO_SERVERS") == null ){
            printUsage();
            System.exit(0);
        }
    }

    private static void printUsage() throws URISyntaxException, IOException {
        System.out.println(new String(Files.readAllBytes(
            Paths.get(ClassLoader.getSystemResource("usage.txt").toURI())),
            Charset.defaultCharset()));
    }
}
